from time import sleep
import subprocess
import os

from guardian import GuardState
from guardian import NodeManager

import cdsutils

#TODO:
# Source @ /opt/rtcds/userapps/release/asc/l1/guardian/optic.py
from optic import Optic

# v1 - S. Aston - 09/07/2017
# Script for LLO to switch all SUS Guardian States.
# Sleeps after asserting node managed avoid having to double down requesting the state.
#
# v2 - S. Aston - 03/28/2018
# Added new squeezer suspensions (ZM1, ZM2), no SUS Guardian yet for OPO or OFI.
# Added new states for corner and ends, EX and EY.
#
# v3 - S. Aston - 06/22/2018
# Added new squeezer suspensions (OPO, OFI), n.b. DACKILLs and IOP DACKILLs have been removed from models.
# Removed tab indentations and replaced with spaces in the RESET_ALL_WD loop.
#
# v4 - S. Aston - 10/05/2018
# Added new Restore All SUS Alignment state to help expedite IFO recovery.
# J. Rollins (most likely?) removed nodes.set_managed() after latest Guardian updates.
#
# v5 - S. Aston - 10/08/2018
# Added new Retore PSL Piezo Alignment state to help expedite IFO recovery.
#
# v6 - S. Aston - 04/25/2019
# Commented out restore PSL PIEZO since Perl support is broken.
# Commented out QUAD_OPLEV_OFF and QUAD_OPLEV_ON for a tidy-up.
#
# v7 - S. Aston - 07/23/2020
# Commented out restore PSL PIEZO since Perl support is still broken.
# Commented out QUAD_OPLEV_OFF and QUAD_OPLEV_ON for a tidy-up.
# Commented out end-station temperarure compensation for a tidy-up.
# Added support for testing MISALIGNED SUS states and tested during Python 2 to Python 3 upgrade then commented out for clarity.
#
# v8 - S. Aston - 04/28/2021
# Added robust and nominal damping for corner suspensions during A+ assembly work.
#
# v9 - S. Aston - 07/14/2022
# Added new SQZ suspensions (HXDS & FC1), still need to add FC2 when ready.
#
# v10 - S. Aston - 11/03/2022
# Added new SQZ suspension FC2.
#
# v11 - A. Mullavey - 03/30/2022
# Using python library optic to do restores

all_suspensions = ['OPO','OFI','ZM1','ZM2','ZM3','ZM4','ZM5','ZM6','FC1','FC2','OMC','OM1','OM2','OM3','RM1','RM2','IM1','IM2','IM3','IM4','TMSX','TMSY',
'BS','SR2','SRM','SR3','PR2','PRM','PR3','MC1','MC2','MC3','ITMX','ITMY','ETMX','ETMY']

corner_suspensions = ['OPO','OFI','ZM1','ZM2','ZM3','ZM4','ZM5','ZM6','FC1','FC2','OMC','OM1','OM2','OM3','RM1','RM2','IM1','IM2','IM3','IM4','BS','SR2',
'SRM','SR3','PR2','PRM','PR3','MC1','MC2','MC3','ITMX','ITMY']

ex_suspensions = ['TMSX','ETMX']

ey_suspensions = ['TMSY','ETMY']

quads = ['ETMX','ITMX','ETMY','ITMY']
quads_corner = ['ITMX','ITMY']

#TODO: test new code, AJM
restorable_optics = {}
for sus in all_suspensions:
    restorable_optics[sus] = Optic(sus)


# nominal operational state
nominal = 'ALL_ALIGNED'

# initial request on initialization
request = 'IDLE'

nodes = NodeManager(['SUS_OPO','SUS_OFI','SUS_ZM1','SUS_ZM2','SUS_ZM3','SUS_ZM4','SUS_ZM5','SUS_ZM6','SUS_FC1','SUS_FC2','SUS_OMC','SUS_OM1','SUS_OM2','SUS_OM3','SUS_RM1','SUS_RM2','SUS_IM1','SUS_IM2','SUS_IM3','SUS_IM4','SUS_TMSX','SUS_TMSY',
'SUS_BS','SUS_SR2','SUS_SRM','SUS_SR3','SUS_PR2','SUS_PRM','SUS_PR3','SUS_MC1','SUS_MC2','SUS_MC3','SUS_ITMX','SUS_ITMY','SUS_ETMX','SUS_ETMY'])


class INIT(GuardState):
    def main(self):
        return True


class IDLE(GuardState):
    goto = True

    def main(self):
        return True


class ALL_SAFE(GuardState):
#    goto = True
    request = True

    def main(self):
        sleep(0.5)
        for sus in all_suspensions:
            nodes['SUS_' + sus] = 'SAFE'
            #nodes['SUS_' + sus] = 'SAFE'

    def run(self):
        if nodes.arrived:
            return True


class ALL_DAMPED(GuardState):
#    goto = True
    request = True

    def main(self):
        sleep(0.5)
        for sus in all_suspensions:
            nodes['SUS_' + sus] = 'DAMPED'
            #nodes['SUS_' + sus] = 'DAMPED'

    def run(self):
        if nodes.arrived:
            return True


class ALL_ALIGNED(GuardState):
#    goto = True
    request = True

    def main(self):
        sleep(0.5)
        for sus in all_suspensions:
            nodes['SUS_' + sus] = 'ALIGNED'
            #nodes['SUS_' + sus] = 'ALIGNED'

    def run(self):
        if nodes.arrived:
            return True


class CORNER_SAFE(GuardState):
#    goto = True
    request = True

    def main(self):
        sleep(0.5)
        for sus in corner_suspensions:
            nodes['SUS_' + sus] = 'SAFE'
            #nodes['SUS_' + sus] = 'SAFE'

    def run(self):
        if nodes.arrived:
            return True


class CORNER_DAMPED(GuardState):
#    goto = True
    request = True

    def main(self):
        sleep(0.5)
        for sus in corner_suspensions:
            nodes['SUS_' + sus] = 'DAMPED'
            #nodes['SUS_' + sus] = 'DAMPED'

    def run(self):
        if nodes.arrived:
            return True


class CORNER_ALIGNED(GuardState):
#    goto = True
    request = True

    def main(self):
        sleep(0.5)
        for sus in corner_suspensions:
            nodes['SUS_' + sus] = 'ALIGNED'
            #nodes['SUS_' + sus] = 'ALIGNED'

    def run(self):
        if nodes.arrived:
            return True


class EX_SAFE(GuardState):
#    goto = True
    request = True

    def main(self):
        sleep(0.5)
        for sus in ex_suspensions:
            nodes['SUS_' + sus] = 'SAFE'
            #nodes['SUS_' + sus] = 'SAFE'

    def run(self):
        if nodes.arrived:
            return True


class EX_DAMPED(GuardState):
#    goto = True
    request = True

    def main(self):
        sleep(0.5)
        for sus in ex_suspensions:
            nodes['SUS_' + sus] = 'DAMPED'
            #nodes['SUS_' + sus] = 'DAMPED'

    def run(self):
        if nodes.arrived:
            return True


class EX_ALIGNED(GuardState):
#    goto = True
    request = True

    def main(self):
        sleep(0.5)
        for sus in ex_suspensions:
            nodes['SUS_' + sus] = 'ALIGNED'
            #nodes['SUS_' + sus] = 'ALIGNED'

    def run(self):
        if nodes.arrived:
            return True


class EY_SAFE(GuardState):
#    goto = True
    request = True

    def main(self):
        sleep(0.5)
        for sus in ey_suspensions:
            nodes['SUS_' + sus] = 'SAFE'
            #nodes['SUS_' + sus] = 'SAFE'

    def run(self):
        if nodes.arrived:
            return True


class EY_DAMPED(GuardState):
#    goto = True
    request = True

    def main(self):
        sleep(0.5)
        for sus in ey_suspensions:
            nodes['SUS_' + sus] = 'DAMPED'
            #nodes['SUS_' + sus] = 'DAMPED'

    def run(self):
        if nodes.arrived:
            return True


class EY_ALIGNED(GuardState):
#    goto = True
    request = True

    def main(self):
        sleep(0.5)
        for sus in ey_suspensions:
            nodes['SUS_' + sus] = 'ALIGNED'
            #nodes['SUS_' + sus] = 'ALIGNED'

    def run(self):
        if nodes.arrived:
            return True

'''
class QUADS_OPLEV_ON(GuardState):
#    goto = True
    request = True
    def main(self):
        
        sleep(0.5)
        for sus in quads_corner:
	    ezca['SUS-' + sus + '_L1_OLDAMP_P_TRAMP']=5
            ezca['SUS-' + sus + '_L1_OLDAMP_P_GAIN']=1

    def run(self):
        return True


class QUADS_OPLEV_OFF(GuardState):
#    goto = True
    request = True
    def main(self):
        
        sleep(0.5)
        for sus in quads:
            ezca['SUS-' + sus + '_L1_OLDAMP_P_TRAMP']=5
            ezca['SUS-' + sus + '_L1_OLDAMP_P_GAIN']=0
    def run(self):
        return True
'''

class CORNER_ROBUST_DAMPED(GuardState):
#    goto = True
    request = True

    def main(self):
        sleep(0.5)
        for sus in corner_suspensions:
            nodes['SUS_' + sus] = 'DAMPED'
            #nodes['SUS_' + sus] = 'DAMPED'
        
        # Set Top-Stage Coil Driver States to 1    
        
        ezca['SUS-ITMX_BIO_M0_STATEREQ'] = 1 # ITMX M0
        ezca['SUS-ITMX_BIO_R0_STATEREQ'] = 1 # ITMX R0

        ezca['SUS-ITMY_BIO_M0_STATEREQ'] = 1 # ITMY M0
        ezca['SUS-ITMY_BIO_R0_STATEREQ'] = 1 # ITMY R0

        ezca['SUS-BS_BIO_M1_STATEREQ'] = 1 # BS M1

        ezca['SUS-MC1_BIO_M1_STATEREQ'] = 1 # MC1 M1
        ezca['SUS-MC2_BIO_M1_STATEREQ'] = 1 # MC2 M1
        ezca['SUS-MC3_BIO_M1_STATEREQ'] = 1 # MC3 M1
 
        ezca['SUS-PR2_BIO_M1_STATEREQ'] = 1 # PR2 M1
        ezca['SUS-PR3_BIO_M1_STATEREQ'] = 1 # PR3 M1
        ezca['SUS-PRM_BIO_M1_STATEREQ'] = 1 # PRM M1

        ezca['SUS-SR2_BIO_M1_STATEREQ'] = 1 # PR2 M1
        ezca['SUS-SR3_BIO_M1_STATEREQ'] = 1 # PR3 M1
        ezca['SUS-SRM_BIO_M1_STATEREQ'] = 1 # PRM M1

        ezca['SUS-OMC_BIO_M1_STATEREQ'] = 1 # OMC M1

        ezca['SUS-IM1_BIO_M1_STATEREQ'] = 1 # IM1 M1
        ezca['SUS-IM2_BIO_M1_STATEREQ'] = 1 # IM2 M1
        ezca['SUS-IM3_BIO_M1_STATEREQ'] = 1 # IM3 M1
        ezca['SUS-IM4_BIO_M1_STATEREQ'] = 1 # IM4 M1
        
        # Set Top-Stage Watchdog thresholds from 8000ish to 80000 

        ezca['SUS-ITMX_M0_WD_OSEMAC_RMS_MAX'] = 80000 # ITMX M0 WD
        ezca['SUS-ITMX_R0_WD_OSEMAC_RMS_MAX'] = 80000 # ITMX R0 WD
        ezca['SUS-ITMX_L1_WD_OSEMAC_RMS_MAX'] = 80000 # ITMX L1 WD
        ezca['SUS-ITMX_L2_WD_OSEMAC_RMS_MAX'] = 80000 # ITMX L2 WD

        ezca['SUS-ITMY_M0_WD_OSEMAC_RMS_MAX'] = 80000 # ITMY M0 WD
        ezca['SUS-ITMY_R0_WD_OSEMAC_RMS_MAX'] = 80000 # ITMY R0 WD
        ezca['SUS-ITMY_L1_WD_OSEMAC_RMS_MAX'] = 80000 # ITMY L1 WD
        ezca['SUS-ITMY_L2_WD_OSEMAC_RMS_MAX'] = 80000 # ITMY L2 WD

        ezca['SUS-BS_M1_WD_OSEMAC_RMS_MAX'] = 80000 # BS M1 WD
        ezca['SUS-BS_M2_WD_OSEMAC_RMS_MAX'] = 80000 # BS M2 WD

        ezca['SUS-MC1_M1_WD_OSEMAC_RMS_MAX'] = 80000 # MC1 M1 WD
        ezca['SUS-MC1_M2_WD_OSEMAC_RMS_MAX'] = 80000 # MC1 M2 WD
        ezca['SUS-MC1_M3_WD_OSEMAC_RMS_MAX'] = 80000 # MC1 M3 WD

        ezca['SUS-MC2_M1_WD_OSEMAC_RMS_MAX'] = 80000 # MC2 M1 WD
        ezca['SUS-MC2_M2_WD_OSEMAC_RMS_MAX'] = 80000 # MC2 M2 WD
        ezca['SUS-MC2_M3_WD_OSEMAC_RMS_MAX'] = 80000 # MC2 M3 WD

        ezca['SUS-MC3_M1_WD_OSEMAC_RMS_MAX'] = 80000 # MC3 M1 WD
        ezca['SUS-MC3_M2_WD_OSEMAC_RMS_MAX'] = 80000 # MC3 M2 WD
        ezca['SUS-MC3_M3_WD_OSEMAC_RMS_MAX'] = 80000 # MC3 M3 WD

        ezca['SUS-PR2_M1_WD_OSEMAC_RMS_MAX'] = 80000 # PR2 M1 WD
        ezca['SUS-PR2_M2_WD_OSEMAC_RMS_MAX'] = 80000 # PR2 M2 WD
        ezca['SUS-PR2_M3_WD_OSEMAC_RMS_MAX'] = 80000 # PR2 M3 WD

        ezca['SUS-PR3_M1_WD_OSEMAC_RMS_MAX'] = 80000 # PR3 M1 WD
        ezca['SUS-PR3_M2_WD_OSEMAC_RMS_MAX'] = 80000 # PR3 M2 WD 
        ezca['SUS-PR3_M3_WD_OSEMAC_RMS_MAX'] = 80000 # PR3 M3 WD 

        ezca['SUS-PRM_M1_WD_OSEMAC_RMS_MAX'] = 80000 # PRM M1 WD
        ezca['SUS-PRM_M2_WD_OSEMAC_RMS_MAX'] = 80000 # PRM M2 WD
        ezca['SUS-PRM_M3_WD_OSEMAC_RMS_MAX'] = 80000 # PRM M3 WD

        ezca['SUS-SR2_M1_WD_OSEMAC_RMS_MAX'] = 80000 # SR2 M1 WD
        ezca['SUS-SR2_M2_WD_OSEMAC_RMS_MAX'] = 80000 # SR2 M2 WD
        ezca['SUS-SR2_M3_WD_OSEMAC_RMS_MAX'] = 80000 # SR2 M3 WD

        ezca['SUS-SR3_M1_WD_OSEMAC_RMS_MAX'] = 80000 # SR3 M1 WD
        ezca['SUS-SR3_M2_WD_OSEMAC_RMS_MAX'] = 80000 # SR3 M2 WD
        ezca['SUS-SR3_M3_WD_OSEMAC_RMS_MAX'] = 80000 # SR3 M3 WD

        ezca['SUS-SRM_M1_WD_OSEMAC_RMS_MAX'] = 80000 # SRM M1 WD
        ezca['SUS-SRM_M2_WD_OSEMAC_RMS_MAX'] = 80000 # SRM M2 WD
        ezca['SUS-SRM_M3_WD_OSEMAC_RMS_MAX'] = 80000 # SRM M3 WD

        ezca['SUS-OMC_M1_WD_OSEMAC_RMS_MAX'] = 80000 # OMC M1 WD
        
    def run(self):
        if nodes.arrived:
            return True

class CORNER_NOMINAL_DAMPED(GuardState):
#    goto = True
    request = True

    def main(self):
        sleep(0.5)
        for sus in corner_suspensions:
            nodes['SUS_' + sus] = 'ALIGNED'
            #nodes['SUS_' + sus] = 'ALIGNED'
        
        # Set Top-Stage Coil Driver States to 2    

        ezca['SUS-ITMX_BIO_M0_STATEREQ'] = 2 # ITMX M0
        ezca['SUS-ITMX_BIO_R0_STATEREQ'] = 2 # ITMX R0

        ezca['SUS-ITMY_BIO_M0_STATEREQ'] = 2 # ITMY M0
        ezca['SUS-ITMY_BIO_R0_STATEREQ'] = 2 # ITMY R0

        ezca['SUS-BS_BIO_M1_STATEREQ'] = 2 # BS M1

        ezca['SUS-MC1_BIO_M1_STATEREQ'] = 2 # MC1 M1
        ezca['SUS-MC2_BIO_M1_STATEREQ'] = 2 # MC2 M1
        ezca['SUS-MC3_BIO_M1_STATEREQ'] = 2 # MC3 M1

        ezca['SUS-PR2_BIO_M1_STATEREQ'] = 2 # PR2 M1
        ezca['SUS-PR3_BIO_M1_STATEREQ'] = 2 # PR3 M1
        ezca['SUS-PRM_BIO_M1_STATEREQ'] = 2 # PRM M1

        ezca['SUS-SR2_BIO_M1_STATEREQ'] = 2 # PR2 M1
        ezca['SUS-SR3_BIO_M1_STATEREQ'] = 2 # PR3 M1
        ezca['SUS-SRM_BIO_M1_STATEREQ'] = 2 # PRM M1

        ezca['SUS-OMC_BIO_M1_STATEREQ'] = 1 # OMC M1

        ezca['SUS-IM1_BIO_M1_STATEREQ'] = 2 # IM1 M1
        ezca['SUS-IM2_BIO_M1_STATEREQ'] = 2 # IM2 M1
        ezca['SUS-IM3_BIO_M1_STATEREQ'] = 2 # IM3 M1
        ezca['SUS-IM4_BIO_M1_STATEREQ'] = 2 # IM4 M1

        # Set Top-Stage Watchdog thresholds from 80000 to 8000ish 

        ezca['SUS-ITMX_M0_WD_OSEMAC_RMS_MAX'] = 8000 # ITMX M0 WD
        ezca['SUS-ITMX_R0_WD_OSEMAC_RMS_MAX'] = 8000 # ITMX R0 WD
        ezca['SUS-ITMX_L1_WD_OSEMAC_RMS_MAX'] = 8000 # ITMX L1 WD
        ezca['SUS-ITMX_L2_WD_OSEMAC_RMS_MAX'] = 8000 # ITMX L2 WD

        ezca['SUS-ITMY_M0_WD_OSEMAC_RMS_MAX'] = 8000 # ITMY M0 WD
        ezca['SUS-ITMY_R0_WD_OSEMAC_RMS_MAX'] = 8000 # ITMY R0 WD
        ezca['SUS-ITMY_L1_WD_OSEMAC_RMS_MAX'] = 8000 # ITMY L1 WD
        ezca['SUS-ITMY_L2_WD_OSEMAC_RMS_MAX'] = 8000 # ITMY L2 WD

        ezca['SUS-BS_M1_WD_OSEMAC_RMS_MAX'] = 8000 # BS M1 WD
        ezca['SUS-BS_M2_WD_OSEMAC_RMS_MAX'] = 10000 # BS M2 WD

        ezca['SUS-MC1_M1_WD_OSEMAC_RMS_MAX'] = 10000 # MC1 M1 WD
        ezca['SUS-MC1_M2_WD_OSEMAC_RMS_MAX'] = 13000 # MC1 M2 WD
        ezca['SUS-MC1_M3_WD_OSEMAC_RMS_MAX'] = 15000 # MC1 M3 WD

        ezca['SUS-MC2_M1_WD_OSEMAC_RMS_MAX'] = 8000 # MC2 M1 WD
        ezca['SUS-MC2_M2_WD_OSEMAC_RMS_MAX'] = 15000 # MC2 M2 WD
        ezca['SUS-MC2_M3_WD_OSEMAC_RMS_MAX'] = 25000 # MC2 M3 WD

        ezca['SUS-MC3_M1_WD_OSEMAC_RMS_MAX'] = 15000 # MC3 M1 WD
        ezca['SUS-MC3_M2_WD_OSEMAC_RMS_MAX'] = 15000 # MC3 M2 WD
        ezca['SUS-MC3_M3_WD_OSEMAC_RMS_MAX'] = 15000 # MC3 M3 WD

        ezca['SUS-PR2_M1_WD_OSEMAC_RMS_MAX'] = 8000 # PR2 M1 WD
        ezca['SUS-PR2_M2_WD_OSEMAC_RMS_MAX'] = 8000 # PR2 M2 WD
        ezca['SUS-PR2_M3_WD_OSEMAC_RMS_MAX'] = 8000 # PR2 M3 WD

        ezca['SUS-PR3_M1_WD_OSEMAC_RMS_MAX'] = 12000 # PR3 M1 WD
        ezca['SUS-PR3_M2_WD_OSEMAC_RMS_MAX'] = 12000 # PR3 M2 WD
        ezca['SUS-PR3_M3_WD_OSEMAC_RMS_MAX'] = 15000 # PR3 M3 WD

        ezca['SUS-PRM_M1_WD_OSEMAC_RMS_MAX'] = 15000 # PRM M1 WD
        ezca['SUS-PRM_M2_WD_OSEMAC_RMS_MAX'] = 15000 # PRM M2 WD
        ezca['SUS-PRM_M3_WD_OSEMAC_RMS_MAX'] = 15000 # PRM M3 WD

        ezca['SUS-SR2_M1_WD_OSEMAC_RMS_MAX'] = 25000 # SR2 M1 WD
        ezca['SUS-SR2_M2_WD_OSEMAC_RMS_MAX'] = 25000 # SR2 M2 WD
        ezca['SUS-SR2_M3_WD_OSEMAC_RMS_MAX'] = 25000 # SR2 M3 WD

        ezca['SUS-SR3_M1_WD_OSEMAC_RMS_MAX'] = 8000 # SR3 M1 WD
        ezca['SUS-SR3_M2_WD_OSEMAC_RMS_MAX'] = 8000 # SR3 M2 WD
        ezca['SUS-SR3_M3_WD_OSEMAC_RMS_MAX'] = 8000 # SR3 M3 WD

        ezca['SUS-SRM_M1_WD_OSEMAC_RMS_MAX'] = 8000 # SRM M1 WD
        ezca['SUS-SRM_M2_WD_OSEMAC_RMS_MAX'] = 8000 # SRM M2 WD
        ezca['SUS-SRM_M3_WD_OSEMAC_RMS_MAX'] = 8000 # SRM M3 WD

        ezca['SUS-OMC_M1_WD_OSEMAC_RMS_MAX'] = 12000 # OMC M1 WD

class RESET_ALL_WD(GuardState):
    goto = True
    request = True

    def main(self):
        for sus in all_suspensions:
            if (('ZM1' in sus) or ('ZM2' in sus) or ('ZM3' in sus) or ('ZM4' in sus) or ('ZM5' in sus) or ('ZM6' in sus) or ('RM1' in sus) or ('RM2' in sus) or ('OM1' in sus) or ('OM2' in sus) or ('OM3' in sus)):
                ezca['SUS-' + sus + '_M1_WD_RSET'] = 1
                ezca['SUS-HTTS_DACKILL_RESET'] = 1
            elif (('IM1' in sus) or ('IM2' in sus) or ('IM3' in sus) or ('IM4' in sus)):
                ezca['SUS-' + sus + '_M1_WD_RSET'] = 1
                ezca['SUS-IM_DACKILL_RESET'] = 1
            elif (('OPO' in sus) or ('OFI' in sus)):
                ezca['SUS-' + sus + '_WD_RESET'] = 1
            else:
                ezca['SUS-' + sus + '_WD_RESET'] = 1
                ezca['SUS-' + sus + '_DACKILL_RESET'] = 1
                ezca['IOP-SUS_' + sus + '_DACKILL_RESET'] = 1

    def run(self):
        return True

'''
class RUN_ENDS_TEMP_COMP(GuardState):
    goto = True
    request = True

    def main(self):
        self.v_ey_start = cdsutils.avg(5, 'SUS-ETMY_M0_DAMP_V_INMON')
        self.v_ex_start = cdsutils.avg(5, 'SUS-ETMX_M0_DAMP_V_INMON')
        ezca['SUS-ETMY_M0_DITHER_P_TRAMP']=3
        ezca['SUS-ETMX_M0_DITHER_P_TRAMP']=3

    def run(self):
        m0_pit_cal = 23.219
        ey_scale   = -1
        ex_scale   = 0.4
        v_ey = cdsutils.avg(10, 'SUS-ETMY_M0_DAMP_V_INMON')
        v_ex = cdsutils.avg(10, 'SUS-ETMX_M0_DAMP_V_INMON')
        ezca['SUS-ETMY_M0_DITHER_P_OFFSET'] = (v_ey-self.v_ey_start)*m0_pit_cal*ey_scale
        ezca['SUS-ETMX_M0_DITHER_P_OFFSET'] = (v_ex-self.v_ex_start)*m0_pit_cal*ex_scale
        return True


class RUN_ENDS_TEMP_COMP_TWO(GuardState):
    goto = True
    request = True

    def main(self):
        self.v_ey_start = cdsutils.avg(5, 'SUS-ETMY_M0_DAMP_V_INMON')
        self.v_ex_start = cdsutils.avg(5, 'SUS-ETMX_M0_DAMP_V_INMON')
        self.v_ty_start = cdsutils.avg(5, 'SUS-TMSY_M1_DAMP_V_INMON')
        self.v_tx_start = cdsutils.avg(5, 'SUS-TMSX_M1_DAMP_V_INMON')
        ezca['SUS-ETMY_M0_DITHER_P_TRAMP']=3
        ezca['SUS-ETMX_M0_DITHER_P_TRAMP']=3
        ezca['SUS-TMSY_M1_LOCK_P_TRAMP']=5
        ezca['SUS-TMSX_M1_LOCK_P_TRAMP']=5

    def run(self):
        m0_pit_cal = 23.219
        m1_pit_cal = 200.00
        ey_scale   = -1.0
        ex_scale   = 0.4
        ty_scale   = 0.8
        tx_scale   = -0.8
        v_ey = cdsutils.avg(10, 'SUS-ETMY_M0_DAMP_V_INMON')
        v_ex = cdsutils.avg(10, 'SUS-ETMX_M0_DAMP_V_INMON')
        v_ty = cdsutils.avg(10, 'SUS-TMSY_M1_DAMP_V_INMON')
        v_tx = cdsutils.avg(10, 'SUS-TMSX_M1_DAMP_V_INMON')
        ezca['SUS-ETMY_M0_DITHER_P_OFFSET'] = (v_ey-self.v_ey_start)*m0_pit_cal*ey_scale
        ezca['SUS-ETMX_M0_DITHER_P_OFFSET'] = (v_ex-self.v_ex_start)*m0_pit_cal*ex_scale
        ezca['SUS-TMSY_M1_LOCK_P_OFFSET'] = (v_ty-self.v_ty_start)*m1_pit_cal*ty_scale
        ezca['SUS-TMSX_M1_LOCK_P_OFFSET'] = (v_tx-self.v_tx_start)*m1_pit_cal*tx_scale
        return True
'''

class RESTORE_ALL_ALIGNMENTS(GuardState):
    goto = True
    request = True

    def main(self):

        #TODO: test new code, AJM
        self.tramp = 10
        for sus in all_suspensions:
            for dof in ['P','Y']:
                if sus=='OFI' and dof=='P':
                    continue
                restorable_optics[sus].optic_align_ramp(dof,self.tramp,ezca)

        sleep(0.1)

        for sus in all_suspensions:
            restorable_optics[sus].restore(ezca)

        #for sus in all_suspensions:
        #    os.system('python /opt/rtcds/userapps/release/sus/common/scripts/align_restore.py' + ' ' + sus)

        self.timer['wait'] = self.tramp

        self.reset_tramp = True

    def run(self):

        if not self.timer['wait']:
            notify('Restoring all suspensions with {}s ramp'.format(self.tramp))
            return

        if self.reset_tramp:
            for sus in all_suspensions:
                for dof in ['P','Y']:
                    if sus=='OFI' and dof=='P':
                        continue
                    restorable_optics[sus].optic_align_ramp(dof,1.0,ezca)
            self.reset_tramp = False

        return True


class SAVE_ALL_ALIGNMENTS(GuardState):
    goto = True
    request = True

    def main(self):

        for sus in all_suspensions:
            restorable_optics[sus].save(ezca)

    def run(self):

        return True


'''        
class ALL_MISALIGNED(GuardState):
#    goto = True
    request = True

    def main(self):
        sleep(0.5)
        for sus in all_suspensions:
            nodes['SUS_' + sus] = 'MISALIGNED'
            #nodes['SUS_' + sus] = 'MISALIGNED'

    def run(self):
        if nodes.arrived:
            return True
'''
'''
class RESTORE_PSL_PIEZO_ALIGNMENTS(GuardState):
    goto = True
    request = True

    def main(self):
        os.system('perl /opt/rtcds/userapps/release/asc/common/scripts/imc/pzt_align_restore')

#    def run(self):
#        return True
'''

edges = [
    ('INIT', 'IDLE'),
    ('IDLE', 'ALL_SAFE'),
    ('IDLE', 'ALL_DAMPED'),
    ('IDLE', 'ALL_ALIGNED'),
    ('IDLE', 'CORNER_SAFE'),
    ('IDLE', 'CORNER_DAMPED'),
    ('IDLE', 'CORNER_ALIGNED'),
    ('IDLE', 'EX_SAFE'),
    ('IDLE', 'EX_DAMPED'),
    ('IDLE', 'EX_ALIGNED'),
    ('IDLE', 'EY_SAFE'),
    ('IDLE', 'EY_DAMPED'),
    ('IDLE', 'EY_ALIGNED'),
#    ('IDLE', 'QUADS_OPLEV_OFF'),
#    ('IDLE', 'QUADS_OPLEV_ON'),
    ('IDLE', 'CORNER_ROBUST_DAMPED'),
    ('IDLE', 'CORNER_NOMINAL_DAMPED'),    
    ('IDLE', 'RESET_ALL_WD'),
    ('IDLE', 'RESTORE_ALL_ALIGNMENTS'),
    ('IDLE', 'SAVE_ALL_ALIGNMENTS'),
#    ('IDLE', 'ALL_MISALIGNED'),
#    ('IDLE', 'RESTORE_PSL_PIEZO_ALIGNMENTS'),
#    ('IDLE', 'RUN_ENDS_TEMP_COMP'),
#    ('IDLE', 'RUN_ENDS_TEMP_COMP_TWO'),
]

edges += [('ALL_SAFE', 'ALL_SAFE')]
edges += [('ALL_DAMPED', 'ALL_DAMPED')]
edges += [('ALL_ALIGNED', 'ALL_ALIGNED')]
edges += [('CORNER_SAFE', 'CORNER_SAFE')]
edges += [('CORNER_DAMPED', 'CORNER_DAMPED')]
edges += [('CORNER_ALIGNED', 'CORNER_ALIGNED')]
edges += [('EX_SAFE', 'EX_SAFE')]
edges += [('EX_DAMPED', 'EX_DAMPED')]
edges += [('EX_ALIGNED', 'EX_ALIGNED')]
edges += [('EY_SAFE', 'EY_SAFE')]
edges += [('EY_DAMPED', 'EY_DAMPED')]
edges += [('EY_ALIGNED', 'EY_ALIGNED')]
#edges += [('QUADS_OPLEV_OFF', 'QUADS_OPLEV_OFF')]
#edges += [('QUADS_OPLEV_ON', 'QUADS_OPLEV_ON')]
edges += [('CORNER_ROBUST_DAMPED', 'CORNER_ROBUST_DAMPED')]
edges += [('CORNER_NOMINAL_DAMPED', 'CORNER_NOMINAL_DAMPED')]
edges += [('RESET_ALL_WD', 'RESET_ALL_WD')]
edges += [('RESTORE_ALL_ALIGNMENTS', 'RESTORE_ALL_ALIGNMENTS')]
edges += [('SAVE_ALL_ALIGNMENTS', 'SAVE_ALL_ALIGNMENTS')]
#edges += [('ALL_MISALIGNED', 'ALL_MISALIGNED')]
#edges += [('RESTORE_PSL_PIEZO_ALIGNMENTS', 'RESTORE_PSL_PIEZO_ALIGNMENTS')]
#edges += [('RUN_ENDS_TEMP_COMP', 'RUN_ENDS_TEMP_COMP')]
#edges += [('RUN_ENDS_TEMP_COMP_TWO', 'RUN_ENDS_TEMP_COMP_TWO')]
